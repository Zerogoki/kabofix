"""KaboFix URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from MainApp.views import index, register, user_login, user_logout, new_problem, my_problems, check, delete, solved, \
    adminka, current, view, comment, like

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),
    path('register', register),
    path('login', user_login),
    path('logout', user_logout),
    path('new', new_problem),
    path('meine', my_problems),
    path('check', check),
    path('del', delete),
    path('solved', solved),
    path('manage', adminka),
    path('current', current),
    path('view', view),
    path('comment', comment),
    path('like', like),
]

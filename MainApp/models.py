from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class Problem(models.Model):
    title = models.TextField()
    body = models.TextField()
    solved = models.BooleanField(default=False)
    progress = models.BooleanField(default=False)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    curator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='problems_curator')
    likes = models.ManyToManyField(User, related_name='problems_like')


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE)
    text = models.TextField()


class Like(models.Model):
    problem = models.ForeignKey(Problem, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

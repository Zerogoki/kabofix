from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from MainApp.models import Problem, Comment

MODER_GROUP_NAME = 'moders'
RESERVED_USERNAME = 'null'


def index(request):
    problems = Problem.objects.filter(solved=False, progress=False)
    return render(request, 'view.html', {'user': request.user, 'problems': problems, 'is_moder': check_moder(request)})


def solved(request):
    problems = Problem.objects.filter(solved=True)
    return render(request, 'view.html', {'user': request.user, 'problems': problems, 'is_moder': check_moder(request),
                                         'solved': True, 'fallback': 'solved'})


def current(request):
    problems = Problem.objects.filter(progress=True)
    return render(request, 'view.html', {'user': request.user, 'problems': problems, 'is_moder': check_moder(request),
                                         'progress': True, 'fallback': 'current'})


def view(request):
    p_id = request.GET.get('id')
    if not p_id:
        return redirect('/')
    if request.method == 'GET':
        p_id = request.GET.get('id')
        problem = Problem.objects.get(pk=p_id)
        likes_count = problem.likes.count()
        comments = Comment.objects.filter(problem=problem)
        liked = request.user in problem.likes.all()
        if (request.user.is_authenticated and request.user == problem.author) or check_moder(request):
            return render(request, 'problem.html',
                          {'problem': problem, 'mode': 'edit', 'comments': comments, 'is_moder': check_moder(request),
                           'logged_in': True, 'user': request.user, 'author': problem.author, 'likes': likes_count,
                           'liked': liked})
        else:
            return render(request, 'problem.html',
                          {'problem': problem, 'mode': 'view', 'comments': comments, 'is_moder': check_moder(request),
                           'logged_in': request.user.is_authenticated, 'likes': likes_count, 'liked': liked})
    if request.method == 'POST':
        problem = Problem.objects.get(pk=p_id)
        if request.user == problem.author or check_moder(request):
            new_text = request.POST.get('body', '')
            if len(new_text) == 0:
                messages.error(request, "Текст не может быть пустым")
            elif new_text == problem.body:
                messages.error(request, "Текст не может быть равен старому")
            else:
                problem.body = new_text
                problem.save()
                messages.add_message(request, messages.SUCCESS, 'Данные обновлены')
            return redirect('/view?id=' + p_id)
        else:
            return redirect('/')


def my_problems(request):
    if not request.user.is_authenticated:
        return redirect('/')
    problems = Problem.objects.filter(author=request.user)
    return render(request, 'view.html', {'user': request.user, 'problems': problems, 'is_moder': check_moder(request)})


def adminka(request):
    if not check_moder(request):
        return redirect('/')
    users = []
    for u in User.objects.all():
        if not u.groups.filter(
                name=MODER_GROUP_NAME).exists() and not u.is_staff and not u.username == RESERVED_USERNAME:
            users.append(u)
    return render(request, 'adminka.html', {'users': users, 'is_moder': True})


def new_problem(request):
    if not request.user.is_authenticated:
        return redirect('/')
    if request.method == 'GET':
        return render(request, 'new.html', {'is_moder': check_moder(request)})
    if request.method == 'POST':
        title = request.POST.get('title')
        body = request.POST.get('body')
        if not title or not body:
            messages.error(request, 'Поля не заполнены')
            return redirect('/new')
        if len(title) > 200 or len(body) > 6000:
            return redirect('/')
        problem = Problem()
        problem.title = title
        problem.body = body
        problem.author = request.user
        problem.curator = User.objects.get(username=RESERVED_USERNAME)
        problem.save()
        return redirect('/')


def register(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('/')
        return render(request, 'register.html')
    if request.method == 'POST':
        username = request.POST.get('username', '').lower()
        password1 = request.POST.get('password1', '')
        password2 = request.POST.get('password2', '')
        if username == '' or password1 == '' or password2 == '':
            messages.error(request, 'Не все поля заполнены')
            return redirect('/register')
        if User.objects.filter(username=username).exists():
            messages.error(request, 'Ник уже занят')
            return redirect('/register')
        if len(username) < 3:
            messages.error(request, 'Слишком короткий ник')
            return redirect('/register')
        if len(username) > 20:
            messages.error(request, 'Слишком длинный ник')
            return redirect('/register')
        if len(password1) < 6:
            messages.error(request, 'Слишком короткий пароль')
            return redirect('/register')
        if len(password1) > 50:
            messages.error(request, 'Слишком длинный пароль')
            return redirect('/register')
        if password1 != password2:
            messages.error(request, 'Пароли не совпадают')
            return redirect('/register')
        else:
            user = User.objects.create_user(username=username, email="null@localdomain.local", password=password1)
            user.save()
            login(request, user)
            return redirect('/')


def user_login(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('/')
        return render(request, 'login.html')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        if username and password and username != RESERVED_USERNAME:
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('/')
        messages.error(request, 'Неправильный логин или пароль')
        return redirect('/login')


def user_logout(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect('/')


def check(request):
    fallback = request.GET.get('fallback', '/')
    if check_moder(request):
        idd = request.GET.get('id')
        a_type = request.GET.get('type')
        if idd and a_type:
            problem = Problem.objects.get(pk=idd)
            if a_type == 'start':
                problem.progress = True
                problem.curator = request.user
            if a_type == 'finish':
                problem.progress = False
                problem.solved = True
            problem.save()
    return redirect(fallback)


def delete(request):
    p_id = request.GET.get('problem_id')
    u_id = request.GET.get('user_id')
    c_id = request.GET.get('comment_id')
    fallback = request.GET.get('fallback', '/')
    if p_id:
        problem = Problem.objects.get(pk=p_id)
        if check_moder(request):
            problem.delete()
        if request.user == problem.author and not problem.solved:
            problem.delete()
    if u_id:
        if check_moder(request):
            user = User.objects.get(pk=u_id)
            if not user.groups.filter(
                    name=MODER_GROUP_NAME).exists() and not user.is_staff and not user.username == RESERVED_USERNAME:
                user.delete()
            return redirect('/manage')
    if c_id:
        comm = Comment.objects.get(pk=c_id)
        if request.user == comm.author or check_moder(request):
            comm.delete()
    return redirect(fallback)


def comment(request):
    p_id = request.GET.get('id')
    if not p_id or not request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        text = request.POST.get('text', '')
        if len(text) == 0:
            messages.error(request, 'Текст комментария не введён')
        else:
            problem = Problem.objects.get(pk=p_id)
            comm = Comment()
            comm.author = request.user
            comm.problem = problem
            comm.text = text
            comm.save()
            messages.add_message(request, messages.SUCCESS, 'Комментарий успешно сохранён')
        return redirect('/view?id=' + p_id)


def like(request):
    p_id = request.GET.get('id')
    fallback = request.GET.get('fallback')
    action = request.GET.get('action')
    if not p_id or not request.user.is_authenticated:
        return redirect('/')
    problem = Problem.objects.get(pk=p_id)
    if action == 'add':
        if request.user in problem.likes.all():
            return redirect('/')
        problem.likes.add(request.user)
    if action == 'remove':
        if request.user not in problem.likes.all():
            return redirect('/')
        problem.likes.remove(request.user)
    return redirect(fallback)


def check_moder(r):
    return r.user.groups.filter(name=MODER_GROUP_NAME).exists()
